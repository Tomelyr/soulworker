namespace Database.Models
{
    public class UsersCharactersModel :  IModel
    {
        public virtual int Id { get; }
        public virtual byte Slot { get; }
        public virtual bool Online { get; set; }
        public virtual int UserId { get; set; }
        public virtual int ServerId { get; set; }
        public virtual int CharacterId { get; set; }
    }
}