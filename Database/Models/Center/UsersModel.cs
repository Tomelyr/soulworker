using System;
using System.Collections.Generic;

namespace Database.Models
{
    public class UsersModel : IModel
    {
        public virtual int Id { get; }
        public virtual string Name { get; }
        public virtual string Password { get; }
        public virtual string AuthCode { get; }
        public virtual string LastAddress { get; set; }
        public virtual string LastMac { get; set; }
        public virtual DateTime LastLogin { get; set; }
        public virtual int LastCharacter { get; set; }
        public virtual IList<UsersAccessModel> Access { get; }
        public virtual IList<UsersCharactersModel> Characters { get; }
    }
}