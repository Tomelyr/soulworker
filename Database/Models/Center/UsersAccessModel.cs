using System;

namespace Database.Models
{
    public class UsersAccessModel : IModel
    {
        public virtual int Id { get; }
        public virtual int UserId { get; set; }
        public virtual DateTime DateTime { get; set; }
        public virtual string Address { get; set; }
        public virtual string Message { get; set; }
    }
}