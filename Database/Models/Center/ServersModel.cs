namespace Database.Models
{
    public class ServersModel : IModel
    {
        public virtual short Id { get; set; }
        public virtual string Name { get; set; }
        public virtual string Address { get; set; }
        public virtual short Port { get; set; }
        public virtual int Count { get; set; }
        public virtual int Limit { get; set; }
        public virtual bool Maintenance { get; set; }
        public virtual bool Online { get; set; }
    }
}