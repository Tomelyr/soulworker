namespace Database.Models
{
    public class CharactersModel : IModel
    {
        public virtual int Id { get; }
        public virtual int UserId { get; }
        public virtual string Name { get; }
        public virtual byte Type { get; }
        public virtual int HairType { get; set; }
        public virtual byte Level { get; set; }
        public virtual UsersCharactersModel UsersCharacter { get; set; }
    }
}