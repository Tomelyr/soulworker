namespace Database
{
    public static class DatabaseAction
    {
        public const string None = "none";
        public const string Update = "update";
        public const string Drop = "drop";
    }
}