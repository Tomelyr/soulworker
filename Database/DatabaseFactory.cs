﻿using System;
using System.IO;
using System.Xml.Serialization;
using Database.Mappings;
using Database.Models;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using FluentNHibernate.Conventions.Helpers;
using Newtonsoft.Json;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Mapping.ByCode;
using NHibernate.Tool.hbm2ddl;

namespace Database
{
    public static class DatabaseFactory
    {
        private static ISessionFactory Session { get; set; }

        private static DatabaseSettings DatabaseSettings { get; set; }

        public static string CENTER => DatabaseSettings.Center;

        public static string GAME => DatabaseSettings.Game;

        public static void Build(string path)
        {
            DatabaseSettings = JsonConvert.DeserializeObject<DatabaseSettings>(File.ReadAllText(path));
        }

        private static IPersistenceConfigurer MySql()
        {
            return MySQLConfiguration.Standard
                .ConnectionString(x =>
                    x.Server(DatabaseSettings.Host)
                        .Username(DatabaseSettings.Username)
                        .Password(DatabaseSettings.Password)
                        .Database(DatabaseSettings.Database)
                );
        }

        private static Configuration GetConfig(IPersistenceConfigurer db)
        {
            return Fluently.Configure()
                .Database(db)
                .Cache(c => c.UseQueryCache().UseSecondLevelCache().UseMinimalPuts())
                .Mappings(m => m.FluentMappings
                    .Add<ServersMap>()
                    .Add<CharactersMap>()
                    .Add<UsersMap>()
                    .Add<UsersCharactersMap>()
                    .Conventions.Add(DefaultCascade.All(), DefaultLazy.Never())) // Lazyload
                .Mappings(m => m.FluentMappings.Add<UsersAccessMap>())
                .BuildConfiguration();
        }

        public static bool Connect()
        {
            try
            {
                var configuration = GetConfig(MySql());
                switch (DatabaseSettings.Action)
                {
                    case DatabaseAction.Update:
                        new SchemaUpdate(configuration).Execute(false, true);
                        break;
                    case DatabaseAction.Drop:
                        new SchemaExport(configuration).Execute(false, false, true);
                        break;
                }

                Session = configuration.BuildSessionFactory();
                return true;
            }
            catch
            {
                return false;
            }
        }

        public static ISession OpenSession()
        {
            return Session.OpenSession();
        }

        public static void Refresh(IModel model)
        {
            using (var session = Session.OpenSession())
            {
                session.Refresh(model);
            }
        }

        public static void Update(IModel model)
        {
            using (var session = Session.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    session.Update(model);
                    transaction.Commit();
                }
            }
        }

        public static void Save(IModel model)
        {
            using (var session = Session.OpenSession())
            {
                using (var transaction = session.BeginTransaction())
                {
                    session.SaveOrUpdate(model);
                    transaction.Commit();
                }
            }
        }
    }
}