﻿namespace Database
{
    internal class DatabaseSettings
    {
        public string Host { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Database { get; set; }
        public string Center { get; set; }
        public string Game { get; set; }
        public string Action { get; set; }
    }
}