using Database.Models;
using FluentNHibernate.Mapping;

namespace Database.Mappings
{
    public class CharactersMap : ClassMap<CharactersModel>
    {
        public CharactersMap()
        {
            Schema(DatabaseFactory.GAME);
            Table("characters");
            Id(x => x.Id).Column("id").Length(11).Unique().GeneratedBy.Increment();
            Map(x => x.UserId).Column("user_id").Length(11).Default("0");
            Map(x => x.Type).Column("char_type").Length(11).Default("0");
            Map(x => x.HairType).Column("hair_type").Length(11).Default("0");
            Map(x => x.Name).Column("name").Length(255).Default("''");
            Map(x => x.Level).Column("level").Length(3).Default("0");
            References(x => x.UsersCharacter, "id").ForeignKey("character_id");
        }
    }
}