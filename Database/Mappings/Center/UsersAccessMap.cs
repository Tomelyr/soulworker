using Database.Models;
using FluentNHibernate.Mapping;

namespace Database.Mappings
{
    public class UsersAccessMap : ClassMap<UsersAccessModel>
    {
        public UsersAccessMap()
        {
            Schema(DatabaseFactory.CENTER);
            Table("users_access");
            Id(x => x.Id).Column("id").Length(11).Unique().GeneratedBy.Increment();
            Map(x => x.UserId).Column("user_id").Length(11).Not.Nullable();
            Map(x => x.Address).Column("address").Length(255).Default("'127.0.0.1'");
            Map(x => x.DateTime).Column("date_time").Not.Nullable();
            Map(x => x.Message).Column("message").Length(255).Not.Nullable();
        }
    }
}