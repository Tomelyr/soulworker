using Database.Models;
using FluentNHibernate.Mapping;

namespace Database.Mappings
{
    public class UsersMap : ClassMap<UsersModel>
    {
        public UsersMap()
        {
            Schema(DatabaseFactory.CENTER);
            Table("users");
            Id(x => x.Id).Column("id").Length(11).Unique().GeneratedBy.Increment();
            Map(x => x.Name).Column("name").Length(255).Default("'Igor'").Not.Nullable();
            Map(x => x.Password).Column("password").Length(255).Not.Nullable();
            Map(x => x.AuthCode).Column("auth_code").Length(255).Not.Nullable();
            Map(x => x.LastAddress).Column("last_address").Length(255).Default("'127.0.0.1'");
            Map(x => x.LastMac).Column("last_mac").Length(255).Default("'00-00-00-00-00-00'");
            Map(x => x.LastLogin).Column("last_login").Not.Nullable();
            Map(x => x.LastCharacter).Column("last_character").Length(11).Default("0");
            HasMany(x => x.Access).KeyColumn("user_id");
            HasMany(x => x.Characters).KeyColumn("user_id");
        }
    }
}