﻿using Database.Models;
using FluentNHibernate.Mapping;

namespace Database.Mappings
{
    public class ServersMap : ClassMap<ServersModel>
    {
        public ServersMap()
        {
            Schema(DatabaseFactory.CENTER);
            Table("servers");
            Id(x => x.Id).Column("id").Length(5).GeneratedBy.Increment();
            Map(x => x.Name).Column("name").Length(255).Default("'Igor'");
            Map(x => x.Address).Column("address").Length(255).Default("'127.0.0.1'");
            Map(x => x.Port).Column("port").Length(5).Default("10100");
            Map(x => x.Count).Column("players_count").Length(11).Default("0");
            Map(x => x.Limit).Column("players_limit").Length(11).Default("100");
            Map(x => x.Maintenance).Column("maintenance").Length(1).Default("0");
            Map(x => x.Online).Column("online").Length(1).Default("0");
        }
    }
}