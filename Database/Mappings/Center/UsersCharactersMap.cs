using Database.Models;
using FluentNHibernate.Mapping;

namespace Database.Mappings
{
    public class UsersCharactersMap : ClassMap<UsersCharactersModel>
    {
        public UsersCharactersMap()
        {
            Schema(DatabaseFactory.CENTER);
            Table("users_characters");
            Id(x => x.Id).Column("id").Length(11).Unique().GeneratedBy.Increment();
            Map(x => x.Slot).Column("slot").Length(3).Default("1");
            Map(x => x.UserId).Column("user_id").Length(11).Not.Nullable();
            Map(x => x.Online).Column("online").Default("0").Length(11).Not.Nullable();
            Map(x => x.CharacterId).Column("character_id").Length(11).Not.Nullable();
            Map(x => x.ServerId).Column("server_id").Length(11).Not.Nullable();
        }
    }
}