﻿using System.Linq;
using Char.Communication.Messages.Out;
using Common.Communication.Messages.Packet;
using Common.Communication.Sessions;

namespace Char.Communication.Messages.In
{
    public class ClientCharacterChangeServerReq : InChannel
    {
        public override void Dispatch(PacketRead read, short keyIdentifier, int sender, Channel channel)
        {
            read.Int();
            var userId = read.Int();
            read.Byte();

            channel.Session.Send(new ClientEnterLoginServerSend(userId, Emulator.ConnectionSettings.Center.Host, (short) Emulator.ConnectionSettings.Center.Port)); // TODO: Load from settings
        }
    }
}