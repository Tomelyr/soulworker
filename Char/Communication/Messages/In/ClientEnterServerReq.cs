﻿using System;
using System.Linq;
using Char.Communication.Messages.Out;
using Common.Communication.Messages.Packet;
using Common.Communication.Sessions;
using Common.Log;
using Database;
using Database.Models;

namespace Char.Communication.Messages.In
{
    public class ClientEnterServerReq : InSession
    {
        public override void Dispatch(PacketRead read, short keyIdentifier, int sender, ISession channelSession)
        {
            // TODO: Somehow check if user is authenticathed
            // Packet Bytes: 41 A5 01 00 01 00 3B 88 C8 00 00 00 00 00 00
            var userId = read.Int();
            var serverId = read.Short();
            Console.WriteLine(userId);
            using (var session = DatabaseFactory.OpenSession())
            {
                var server = session.QueryOver<ServersModel>().Where(x => x.Id == serverId).SingleOrDefault();
                if (server == null) return;
                var user = session.QueryOver<UsersModel>().Where(x => x.Id == userId).SingleOrDefault();
                if (user == null) return;
                using (var transaction = session.BeginTransaction())
                {
                    user.LastAddress = channelSession.Address;
                    user.Access.Add(new UsersAccessModel
                    {
                        UserId = user.Id,
                        DateTime = DateTime.Now,
                        Address = channelSession.Address,
                        Message = "Character access."
                    });
                    session.Update(user);
                    transaction.Commit();
                }

                channelSession.Send(new ClientEnterServerRes(0, userId));
                channelSession.Send(new ClientWorldCurDateSend());
                channelSession.Register(new Channel(channelSession, user));
                Logging.User("authenticated successfully", user.Name);
            }
        }
    }
}