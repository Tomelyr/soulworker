﻿using System.Linq;
using Char.Communication.Messages.Out;
using Common.Communication.Messages.Packet;
using Common.Communication.Sessions;

namespace Char.Communication.Messages.In
{
    public class ClientCharacterListReq : InChannel
    {
        public override void Dispatch(PacketRead read, short keyIdentifier, int sender, Channel channel)
        {
            channel.Session.Send(new ClientCharacterListSend(channel.Data));
        }
    }
}