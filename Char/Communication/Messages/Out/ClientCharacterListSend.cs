﻿using System.Linq;
using Common.Communication.Messages;
using Common.Communication.Messages.Packet;
using Database;
using Database.Models;

namespace Char.Communication.Messages.Out
{
    public class ClientCharacterListSend : PacketOut
    {
        public ClientCharacterListSend(UsersModel usersModel)
            : base(Opcodes.ClientCharacterListSend)
        {
            // TODO
            
            Write.Byte((byte) usersModel.Characters.Count);
            foreach (var userCharacter in usersModel.Characters.Where(x => x.ServerId == Emulator.ConnectionSettings.ServerId))
            {
                using (var session = DatabaseFactory.OpenSession())
                {
                    var character = session.QueryOver<CharactersModel>().Where(x => x.UserId == usersModel.Id).Where(x => x.Id == userCharacter.CharacterId).SingleOrDefault();
                    if (character == null) continue;
                    Write.Int(character.Id);
                    SBasicCharInfo(character.Name, character.Type, character.HairType);
                    Write.Byte(character.Level);
                    Write.Byte(0);  // unk2
                    Write.Int(0);   // unk3
                    Write.Byte(0);  // unk4
                    Write.Int(0);   // unk5
                    Write.Byte(0);  // unk6
                    Write.Int(0);   // unk7
                    Write.Byte(0);  // unk8
                    Write.Int(-1);   // unk9
                    for (var i = 0; i < 13; i++)
                        unkStruct1();   // unk10[13]
                    Write.Int(0);   // unk11
                    Write.Int(0);   // unk12
                    Write.Int(0);   // unk13
                    Write.Int(0);   // unk14
                    Write.Str(""); // unk15
                    Write.Int(0);   // unk16
                    unkStruct2();   // unk17
                    Write.Byte(0);  // unk18
                    Write.Str(""); // unk19
                    Write.Short(0); // unk20
                    Write.Short(0); // unk21
                    Write.Short(0); // unk22
                    Write.Byte(0);  // unk23
                    Write.Int(0);   // unk24
                    Write.Byte(0);  // unk25
                    Write.Int(0);   // unk26
                    Write.Byte(0);  // unk27_len
                    //unkStruct3();   // unk27[unk27_len] (já que unk27_len é 0, unk27 não é chamado)
                    Write.Byte(userCharacter.Slot);
                }
            }
            Write.Int(usersModel.LastCharacter);  // Selected Character ID
            Write.Byte(0); // unk1
            Write.Byte(0); // unk2
            Write.Long(0); // unk3
        }

        private void SBasicCharInfo(string charName, byte charType, int hairType)
        {
            Write.UnicodeStr(charName);    // WSTRING
            Write.Byte(charType);
            Write.Byte(0);         // unk1
            Write.Long(hairType);  // hairType (2 bytes) + colors (6 bytes)
            Write.Long(0);         // unk3
        }

        private void unkStruct1()  // unkStruct1 unk10[13] // Igoor: 13 é o tamanho da array.
        {
            Write.Int(-1);
            Write.Int(-1);
        }

        private void unkStruct2() // 2 float
        {
            Write.Int(0);   // unk1
            Write.Int(0);   // unk2
            Write.Int(0);   // unk3
            Write.Int(0);   // unk4
            Write.Int(0);   // unk5
            Write.Int(0);   // unk6
            Write.Int(0);   // unk7
            Write.Int(0);   // unk8
            Write.Int(0);   // unk9
            Write.Int(0);   // unk10
            Write.Float(1f); // float 1
            Write.Float(1f); // float 2
        }

        private void unkStruct3()
        {
            Write.Int(0);   // unk1
            Write.Float(0f); // float
            Write.Byte(0);  // unk3
            Write.Int(0);   // unk4
            Write.Byte(0);  // unk5
        }
    }
}