﻿using Common.Communication.Messages;
using Common.Communication.Messages.Packet;
using Database.Models;

namespace Char.Communication.Messages.Out
{
    public class ClientSystemServerOptionUpdateSend : PacketOut
    {
        public ClientSystemServerOptionUpdateSend(UsersModel usersModel)
            : base(Opcodes.ClientSystemServerOptionUpdate)
        {
            // TODO
            Write.ArrayBytes(new byte[] { 0x01, 0x00, 0x01, 0x00, 0x01, 0x01, 0x00, 0x00, 0x01, 0x00, 0x00, 0x01, 0x00, 0x01 });
        }
    }
}