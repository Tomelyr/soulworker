﻿using Common.Communication.Messages;
using Common.Communication.Messages.Packet;
using System;

namespace Char.Communication.Messages.Out
{
    public class ClientWorldCurDateSend : PacketOut
    {
        public ClientWorldCurDateSend()
            : base(Opcodes.ClientWorldCurDateSend)
        {
            var now = DateTime.UtcNow;
            Write.Long((now.Ticks - new DateTime(1970, 1, 1, 0, 0, 0).Ticks) / 10000000);
            Write.Short((short)now.Year);
            Write.Short((short)now.Month);
            Write.Short((short)now.Day);
            Write.Short((short)now.Hour);
            Write.Short((short)now.Minute);
            Write.Short((short)now.Second);
            Write.Bool(TimeZone.CurrentTimeZone.IsDaylightSavingTime(now));
        }
    }
}