﻿using Common.Communication.Messages;
using Common.Communication.Messages.Packet;

namespace Char.Communication.Messages.Out
{
    public class ClientEnterLoginServerSend : PacketOut
    {
        public ClientEnterLoginServerSend(int userId, string serverAddress, short serverPort)
            : base(Opcodes.ClientCharaterChangeServer)
        {
            Write.Int(0);
            Write.Int(userId);
            Write.Str(serverAddress);
            Write.Short(serverPort);
            Write.Byte(0);
            Write.Byte(1);
        }
    }
}