﻿using Common.Communication.Messages;
using Common.Communication.Messages.Packet;

namespace Char.Communication.Messages.Out
{
    public class ClientEnterServerRes : PacketOut
    {
        public ClientEnterServerRes(byte result, int userId)
            : base(Opcodes.ClientEnterServerSend)
        {
            Write.Byte(result);
            Write.Int(userId);
        }
    }
}