﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Char.Communication.Messages.In;
using Common.Communication;
using Common.Communication.Messages;
using Common.Game;
using Common.Log;
using Common.Server;
using Common.Settings;
using Common.Utilities;
using Database;

namespace Char
{
    public static class Emulator
    {
        private static readonly Server Server = new Server();
        
        public static ConnectionSettings ConnectionSettings { get; private set; }

        public static void Main(string[] args)
        {
            Console.Title = "Soul Worker Char";
            DatabaseFactory.Build(Path.Combine(Application.StartupPath, @"data\database.json"));
            if (DatabaseFactory.Connect())
            {
                Server.Build(new Settings<ServerSettings>().Build(Path.Combine(Application.StartupPath, @"data\network.json")));
                ConnectionSettings = new Settings<ConnectionSettings>().Build(Path.Combine(Application.StartupPath, @"data\connection.json"));
                Logging.Server($"[{ConnectionSettings.ServerId}] Center: {ConnectionSettings.Center.Host}:{ConnectionSettings.Center.Port}");
                Logging.Server($"[{ConnectionSettings.ServerId}] Game: {ConnectionSettings.Game.Host}:{ConnectionSettings.Game.Port}");
                ServerPackets.Register(Opcodes.ClientEnterServerReq, new ClientEnterServerReq());
                ServerPackets.Register(Opcodes.ClientCharaterChangeServer, new ClientCharacterChangeServerReq());
                ServerPackets.Register(Opcodes.ClientCharacterListReq, new ClientCharacterListReq());
                Server.Start();
            }

            while (true) Thread.Sleep(50);
        }
    }
}