using System.Linq;
using Center.Center;
using Center.Communication.Messages.Out;
using Common.Communication.Messages.Packet;
using Common.Communication.Sessions;

namespace Center.Communication.Messages.In
{
    public class ClientServerConnectChanReq : InChannel
    {
        public override void Dispatch(PacketRead read, short keyIdentifier, int sender, Channel channel)
        {
            var servers = CenterData.GetServers();
            var serverId = read.Short();
            var server = servers.SingleOrDefault(x => x.Id == serverId);
            if (server == null) return;
            channel.Session.Send(new ClientServerConnectSend(server.Address, (short) server.Port));
        }
    }

    public class ClientServerConnectReq : InSession
    {
        public override void Dispatch(PacketRead read, short keyIdentifier, int sender, ISession session)
        {
            var servers = CenterData.GetServers();
            var serverId = read.Short();
            var server = servers.SingleOrDefault(x => x.Id == serverId);
            if (server == null) return;
            session.Send(new ClientServerConnectSend(server.Address, (short) server.Port));
        }
    }
}