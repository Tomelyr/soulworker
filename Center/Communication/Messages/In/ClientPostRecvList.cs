﻿using Common.Communication.Messages.Packet;
using Common.Communication.Sessions;

namespace Center.Communication.Messages.In
{
    public class ClientPostRecvList : InSession
    {
        public override void Dispatch(PacketRead read, short keyIdentifier, int sender, ISession channelSession)
        {
            // this packet doesn't have a response
        }
    }
}