using System;
using Center.Communication.Messages.Out;
using Common.Communication.Messages.Packet;
using Common.Communication.Sessions;
using Common.Log;
using Database;
using Database.Models;

namespace Center.Communication.Messages.In
{
    public class ClientGameForgeLogin : InSession
    {
        public override void Dispatch(PacketRead read, short keyIdentifier, int sender, ISession channelSession)
        {
            // TODO
            var authCode = read.UnicodeString();
            var mac = read.String();
            using (var session = DatabaseFactory.OpenSession())
            {
                var user = session.QueryOver<UsersModel>().Where(x => x.AuthCode == authCode).SingleOrDefault();
                if (user == null) return;
                using (var transaction = session.BeginTransaction())
                {
                    user.LastAddress = channelSession.Address;
                    user.LastLogin = DateTime.Now;
                    user.LastMac = mac;
                    user.Access.Add(new UsersAccessModel
                    {
                        UserId = user.Id,
                        DateTime = DateTime.Now,
                        Address = channelSession.Address,
                        Message = "Center access"
                    });
                    session.Update(user);
                    transaction.Commit();
                }

                channelSession.Send(new ClientLoginResultSend(user, mac));
                channelSession.Register(new Channel(channelSession, user));
                Logging.User("authenticated successfully", user.Name);
            }
        }
    }
}