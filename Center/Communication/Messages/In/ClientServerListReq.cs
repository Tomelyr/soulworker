﻿using Center.Communication.Messages.Out;
using Common.Communication.Messages.Packet;
using Common.Communication.Sessions;
using Database;
using Database.Models;

namespace Center.Communication.Messages.In
{
    public class ClientServerListChanReq : InChannel
    {
        public override void Dispatch(PacketRead read, short keyIdentifier, int sender, Channel channel)
        {
            channel.Session.Send(new ClientServerListSend(channel.Data));
            channel.Session.Send(new ClientOptionLoadSend());
        }
    }

    public class ClientServerListReq : InSession
    {
        public override void Dispatch(PacketRead read, short keyIdentifier, int sender, ISession session)
        {
            var userId = read.Int();
            using (var dbsession = DatabaseFactory.OpenSession())
            {
                var user = dbsession.QueryOver<UsersModel>().Where(x => x.Id == userId).SingleOrDefault();
                if (user == null) return;
                session.Send(new ClientServerListSend(user));
                session.Send(new ClientOptionLoadSend());
            }
        }
    }
}