﻿using System.Text;
using Common.Communication.Messages;
using Common.Communication.Messages.Packet;
using Database.Models;

namespace Center.Communication.Messages.Out
{
    public class ClientLoginResultSend : PacketOut
    {
        public ClientLoginResultSend(UsersModel usersModel, string mac)
            : base(Opcodes.ClientLoginResult)
        {
            // TODO
            Write.Int(usersModel.Id);
            Write.Byte(1);
            Write.ArrayBytes(Encoding.ASCII.GetBytes(mac));
            Write.ArrayBytes(new byte[7]);
            Write.Byte(4);
            Write.UnicodeStr(usersModel.Name);
            Write.ArrayBytes(new byte[] {0x3B, 0x88, 0xC8, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00});
        }
    }
}