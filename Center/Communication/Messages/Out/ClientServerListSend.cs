﻿using System.Linq;
using Center.Center;
using Common.Communication.Messages;
using Common.Communication.Messages.Packet;
using Common.Utilities;
using Database.Models;

namespace Center.Communication.Messages.Out
{
    public class ClientServerListSend : PacketOut
    {
        public ClientServerListSend(UsersModel usersModel)
            : base(Opcodes.ClientServerListSend)
        {
            var servers = CenterData.GetServersOnline();
            Write.Byte(0);
            Write.Byte((byte) servers.Length);
            foreach (var server in servers)
            {
                Write.Short(server.Id);
                Write.Short(server.Port);
                Write.Str(server.Name);
                Write.Str(server.Address);

                Write.Int(server.Maintenance ? 0 : ServerStatus.Status(server.Count, server.Limit));
                Write.Int(0);

                Write.Byte((byte) usersModel.Characters.Count(x => x.ServerId == server.Id)); // characters
            }
        }
    }
}