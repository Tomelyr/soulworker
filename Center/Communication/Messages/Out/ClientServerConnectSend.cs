﻿using Common.Communication.Messages;
using Common.Communication.Messages.Packet;

namespace Center.Communication.Messages.Out
{
    public class ClientServerConnectSend : PacketOut
    {
        public ClientServerConnectSend(string serverAddress, short serverPort)
            : base(Opcodes.ClientServerConnectSend)
        {
            Write.Str(serverAddress);
            Write.Short(serverPort);
        }
    }
}