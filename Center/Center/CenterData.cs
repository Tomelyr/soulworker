using System.Collections.Generic;
using System.Linq;
using System.Net.Sockets;
using System.Threading;
using Common.Log;
using Database;
using Database.Models;
using NHibernate;

namespace Center.Center
{
    public static class CenterData
    {
        private static Thread _status;
        
        private static IList<ServersModel> Servers { get; set; }

        public static void Build()
        {
            Logging.Info("Loading center data...");

            using (var session = DatabaseFactory.OpenSession())
            {
                Servers = session.QueryOver<ServersModel>().List();
            }
        }

        public static void Run()
        {
            if (_status != null) return;
            Logging.Server("Starting game servers checker...");
            _status = new Thread(() =>
            {
                while (_status.IsAlive)
                {
                    foreach (var server in Servers)
                    {
                        DatabaseFactory.Refresh(server);
                        var tcpClient = new TcpClient();
                        try
                        {
                            tcpClient.Connect(server.Address, server.Port);
                            if (!server.Online) 
                                Logging.Server($"{server.Name} online.");
                            server.Online = true;
                            tcpClient.Close();
                        }
                        catch
                        {
                            if (server.Online) 
                                Logging.Server($"{server.Name} offline.");
                            server.Online = false;
                        }
                        DatabaseFactory.Update(server);
                    }
                    Thread.Sleep(2000);
                }
            });
            _status.Start();
        }

        public static ServersModel[] GetServersOnline()
        {
            return Servers.Where(x => x.Online).ToArray();
        }

        public static ServersModel[] GetServers()
        {
            return Servers.ToArray();
        }
    }
}