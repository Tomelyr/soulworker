﻿using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;
using Center.Center;
using Center.Communication.Messages.In;
using Common.Communication;
using Common.Communication.Messages;
using Common.Server;
using Common.Settings;
using Common.Utilities;
using Database;
using Database.Models;

namespace Center
{
    public static class Emulator
    {
        private static readonly Server Server = new Server();

        public static void Main(string[] args)
        {
            Console.Title = "Soul Worker Center";
            DatabaseFactory.Build(Path.Combine(Application.StartupPath, @"data\database.json"));
            if (DatabaseFactory.Connect())
            {
                Server.Build(new Settings<ServerSettings>().Build(Path.Combine(Application.StartupPath, @"data\network.json")));
                CenterData.Build();
                CenterData.Run();
                ServerPackets.Register(Opcodes.ClientPostRecvList, new ClientPostRecvList());
                ServerPackets.Register(Opcodes.ClientLoginForGf, new ClientGameForgeLogin());
                ServerPackets.Register(Opcodes.ClientServerListReq, new ClientServerListReq());
                ServerPackets.Register(Opcodes.ClientServerConnectReq, new ClientServerConnectReq());
                ServerPackets.Register(Opcodes.ClientServerListReq, new ClientServerListChanReq());
                ServerPackets.Register(Opcodes.ClientServerConnectReq, new ClientServerConnectChanReq());
                Server.Start();
            }

            while (true) Thread.Sleep(50);
        }
    }
}