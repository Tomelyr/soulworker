﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Windows.Forms;
using Common.Communication.Messages;
using Common.Communication.Sessions;
using Common.Crypto;
using Common.Log;
using Common.Settings;
using Common.Utilities;
using Newtonsoft.Json;

namespace Common.Server
{
    public class Server
    {
        private readonly DateTime _serverStarted = DateTime.Now;

        private TcpListener TcpListener { get; set; }

        private ServerSettings ServerSettings { get; set; }

        public void Build(ServerSettings settings)
        {
            Cryptography.Load(Path.Combine(Application.StartupPath, @"data/key_table"));
            ServerSettings = settings;
        }

        public void Start()
        {
            try
            {
                Logging.Info($"Loaded: {ServerPackets.SessionCount} session packets.");
                Logging.Info($"Loaded: {ServerPackets.ChannelCount} channel packets.");
                TcpListener = new TcpListener(IPAddress.Parse(ServerSettings.Host), ServerSettings.Port);
                TcpListener.Start();
                TcpListener.Server.Listen(ServerSettings.Backlog);
                TcpListener.BeginAcceptTcpClient(AcceptConnection, null);
                Logging.Info($"Waiting connections on {ServerSettings.Host}:{ServerSettings.Port}");
                var timeUsed = DateTime.Now - _serverStarted;
                Logging.Info($"Server ready in ({timeUsed.Seconds} s, {timeUsed.Milliseconds} ms)");
            }
            catch (Exception ex)
            {
                Logging.Alert($"ERROR: {ex.Message}");
            }
        }

        private void AcceptConnection(IAsyncResult iAsyncResult)
        {
            var endAcceptSocket = TcpListener.EndAcceptSocket(iAsyncResult);
            var serverSession = new Session(endAcceptSocket);
            ThreadPool.QueueUserWorkItem(serverSession.Receive);
            TcpListener.BeginAcceptTcpClient(AcceptConnection, null);
        }
    }
}