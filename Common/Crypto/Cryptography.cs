﻿using System.IO;
using Common.Log;

namespace Common.Crypto
{
    public static class Cryptography
    {
        private static byte[] KeyTable { get; set; }

        public static void Load(string filePath)
        {
            Logging.Info("Loading cryptography...");
            KeyTable = File.ReadAllBytes(filePath);

            if (KeyTable.Length < 64)
                Logging.Alert($"Key table has an strange length ({KeyTable.Length})!");
        }

        public static byte[] CryptBytes(short keyIdentifier, byte[] bytes)
        {
            for (var i = 0; i < bytes.Length; i++)
                bytes[i] ^= KeyTable[4 * keyIdentifier - 3 * (int) ((float) i / 3) + i];
            return bytes;
        }

        public static void CryptBytes(short keyIdentifier, ref byte[] bytes, int offset)
        {
            for (var i = 0; i < bytes.Length - offset; i++)
                bytes[offset + i] ^= KeyTable[4 * keyIdentifier - 3 * (int) ((float) i / 3) + i];
        }
    }
}