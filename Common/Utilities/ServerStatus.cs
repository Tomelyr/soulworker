using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;
using Common.Log;
using Database;

namespace Common.Utilities
{
    public static class ServerStatus
    {
        
        public static int Status(int count, int limit)
        {
            var percentage = (int) (0.5f + 100f * count / limit);
            return percentage >= 100 ? 4 : percentage >= 66.66 ? 3 : percentage >= 33.33 ? 2 : 1;
        }
    }
}