﻿namespace Common.Communication.Messages.Packet
{
    public abstract class PacketOut
    {
        public PacketOut(Opcodes opcode) : this((short) opcode)
        {
        }

        public PacketOut(short opcode)
        {
            Write.Short(opcode, true);
        }

        public int Length => Write.Length;

        public byte[] Buffer => Write.Buffer;

        public PacketWrite Write { get; } = new PacketWrite();
    }
}