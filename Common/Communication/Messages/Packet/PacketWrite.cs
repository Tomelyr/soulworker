﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using Common.Utilities;

namespace Common.Communication.Messages.Packet
{
    public class PacketWrite
    {
        public const int HeaderLength = sizeof(short) + sizeof(short) + sizeof(byte);

        private readonly List<byte> _byteList;

        public PacketWrite()
        {
            _byteList = new List<byte>();
        }

        public PacketWrite(int capacity)
        {
            _byteList = new List<byte>(capacity);
        }

        public byte[] Buffer => _byteList.ToArray();

        public int Length => _byteList.Count;

        public void SByte(sbyte @sbyte)
        {
            Include((byte)@sbyte);
        }

        public void Byte(byte @byte)
        {
            Include(@byte);
        }

        public void Bool(bool boolean)
        {
            Include((byte)(boolean ? 1 : 0));
        }

        public void Short(short int16, bool reverse = false)
        {
            Include(BitConverter.GetBytes(reverse ? (short)((int16 << 8) + (int16 >> 8)) : int16));
        }

        public void UShort(ushort uint16)
        {
            Include(BitConverter.GetBytes(uint16));
        }

        public void Int(int int32)
        {
            Include(BitConverter.GetBytes(int32));
        }

        public void UInt(uint uint32)
        {
            Include(BitConverter.GetBytes(uint32));
        }

        public void Long(long int64)
        {
            Include(BitConverter.GetBytes(int64));
        }

        public void ULong(ulong uint64)
        {
            Include(BitConverter.GetBytes(uint64));
        }

        public void Float(float single)
        {
            Include(BitConverter.GetBytes(single));
        }

        public void Double(double @double)
        {
            Include(BitConverter.GetBytes(@double));
        }

        public void UnicodeStr(string ustr)
        {
            if (ustr.Length == 0)
            {
                UShort(0);
                return;
            }
            var bytes = Encoding.Unicode.GetBytes(ustr);
            UShort((ushort)bytes.Length);
            Include(bytes);
        }

        public void Str(string str)
        {
            if (str.Length == 0)
            {
                UShort(0);
                return;
            }
            var bytes = Encoding.ASCII.GetBytes(str);
            UShort((ushort)bytes.Length);
            Include(bytes);
        }

        public void ArrayBytes(byte[] bytes)
        {
            Include(bytes);
        }

        public void HexArray(string bytes)
        {
            if (bytes.Length % 2 == 0)
                throw new Exception("Invalid hex format");
            bytes = bytes.Replace(" ", "").Replace("-", "");
            for (var i = 0; i < bytes.Length / 2; i++)
                Byte(byte.Parse(bytes.Substring(i * 2, 2), NumberStyles.HexNumber));
        }

        private void Include(byte data)
        {
            _byteList.Add(data);
        }

        private void Include(byte[] data)
        {
            _byteList.AddRange(data);
        }
    }
}