using Common.Communication.Sessions;

namespace Common.Communication.Messages.Packet
{
    public abstract class InChannel
    {
        public abstract void Dispatch(PacketRead read, short keyIdentifier, int sender, Channel channel);
    }
}