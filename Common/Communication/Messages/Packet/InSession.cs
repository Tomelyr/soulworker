﻿using Common.Communication.Sessions;

namespace Common.Communication.Messages.Packet
{
    public abstract class InSession
    {
        public abstract void Dispatch(PacketRead read, short keyIdentifier, int sender, ISession session);
    }
}