﻿using System;
using System.Text;
using Common.Utilities;

namespace Common.Communication.Messages.Packet
{
    public class PacketRead
    {
        public const int HeaderLength = sizeof(short) + sizeof(short) + sizeof(byte);

        private int _position;

        public PacketRead(byte[] buffer)
        {
            Buffer = buffer;
        }

        public byte[] Buffer { get; }

        public int Length => Buffer.Length;

        public int Remaining => Buffer.Length - _position;

        public byte[] ArrayBytes(int size)
        {
            var currentBuffer = new byte[size];
            Array.Copy(Buffer, _position, currentBuffer, 0, size);
            _position += size;
            return currentBuffer;
        }

        public void Jump(int size)
        {
            _position += size;
        }

        public byte Byte()
        {
            return Buffer[_position++];
        }

        public bool Bool()
        {
            return Buffer[_position++] == 1;
        }

        public int Int()
        {
            _position += sizeof(int);
            return BitConverter.ToInt32(Buffer, _position - sizeof(int));
        }

        public uint UInt()
        {
            _position += sizeof(uint);
            return BitConverter.ToUInt32(Buffer, _position - sizeof(uint));
        }

        public short Short()
        {
            _position += sizeof(short);
            return BitConverter.ToInt16(Buffer, _position - sizeof(short));
        }

        public ushort UShort()
        {
            _position += sizeof(ushort);
            return BitConverter.ToUInt16(Buffer, _position - sizeof(ushort));
        }

        public long Long()
        {
            _position += sizeof(long);
            return BitConverter.ToInt64(Buffer, _position - sizeof(long));
        }

        public ulong ULong()
        {
            _position += sizeof(ulong);
            return BitConverter.ToUInt64(Buffer, _position - sizeof(ulong));
        }

        public float Float()
        {
            _position += sizeof(float);
            return BitConverter.ToSingle(Buffer, _position - sizeof(float));
        }

        public double Double()
        {
            _position += sizeof(double);
            return BitConverter.ToDouble(Buffer, _position - sizeof(double));
        }

        public string UnicodeString()
        {
            var sizeUString = UShort();
            if (sizeUString == 0)
                return string.Empty;
            _position += sizeUString;
            return Encoding.Unicode.GetString(Buffer, _position - sizeUString, sizeUString);
        }

        public string String()
        {
            var sizeString = UShort();
            if (sizeString == 0)
                return string.Empty;
            _position += sizeString;
            return Encoding.ASCII.GetString(Buffer, _position - sizeString, sizeString);
        }
    }
}