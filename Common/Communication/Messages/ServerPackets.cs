﻿using System.Collections.Generic;
using Common.Communication.Messages.Packet;

namespace Common.Communication.Messages
{
    public static class ServerPackets
    {
        private static readonly Dictionary<short, InSession> SessionPacketIns = new Dictionary<short, InSession>();
        private static readonly Dictionary<short, InChannel> ChannelPacketIns = new Dictionary<short, InChannel>();

        public static int SessionCount => SessionPacketIns.Count;

        public static int ChannelCount => ChannelPacketIns.Count;

        public static void Register(Opcodes code, InSession packet)
        {
            Register((short) code, packet);
        }

        public static void Register(short code, InSession packet)
        {
            if (SessionPacketIns.ContainsKey(code)) return;
            SessionPacketIns.Add(code, packet);
        }

        public static void Register(Opcodes code, InChannel packet)
        {
            Register((short) code, packet);
        }

        public static void Register(short code, InChannel packet)
        {
            if (ChannelPacketIns.ContainsKey(code)) return;
            ChannelPacketIns.Add(code, packet);
        }

        public static bool TryPacket(Opcodes code, out InSession packet)
        {
            return TryPacket((short) code, out packet);
        }

        public static bool TryPacket(short code, out InSession packet)
        {
            return SessionPacketIns.TryGetValue(code, out packet);
        }

        public static bool TryPacket(Opcodes code, out InChannel packet)
        {
            return TryPacket((short) code, out packet);
        }

        public static bool TryPacket(short code, out InChannel packet)
        {
            return ChannelPacketIns.TryGetValue(code, out packet);
        }
    }
}