﻿using System;
using System.Net.Sockets;
using Common.Communication.Messages;
using Common.Communication.Messages.Packet;
using Common.Crypto;
using Common.Log;

namespace Common.Communication.Sessions
{
    public class Session : ISession
    {
        private bool _running = true;

        public Session(Socket socket)
        {
            authenticated = false;
            Socket = socket;
        }

        private Socket Socket { get; }

        private Channel Channel { get; set; }

        private bool authenticated { get; set; }

        public string Address => Socket.RemoteEndPoint.ToString().Split(':')[0];

        public void Send(PacketOut _out)
        {
            try
            {
                var packetSize = PacketWrite.HeaderLength + _out.Length;
                var packet = new PacketWrite(packetSize);
                packet.Short(2);
                packet.Short((short) packetSize);
                packet.Byte(1);
                packet.ArrayBytes(Cryptography.CryptBytes(2, _out.Buffer));
                Socket.Send(packet.Buffer, 0, packetSize, 0);
            }
            catch (Exception e)
            {
                Shutdown();
            }
        }

        public void Register(Channel channel)
        {
            if (authenticated) return;
            Channel = channel;
            authenticated = true;
        }

        public void Receive(object state)
        {
            var buffer = new byte[1024];
            while (_running)
                try
                {
                    if (Socket.Receive(buffer, 0, buffer.Length, 0) > 4)
                    {
                        var packetRead = new PacketRead(buffer);
                        var keyIdentifier = packetRead.Short();
                        var size = packetRead.Short();
                        var sender = packetRead.Byte();
                        Cryptography.CryptBytes(keyIdentifier, ref buffer, PacketRead.HeaderLength);

                        var code1 = packetRead.Byte();
                        var code2 = packetRead.Byte();
                        var codes = (Opcodes) (code2 + (code1 << 8));
                        Console.WriteLine(codes);
                        if (!authenticated)
                        {
                            if (ServerPackets.TryPacket(codes, out InSession inSession))
                            {
                                inSession.Dispatch(packetRead, keyIdentifier, sender, this);
                                continue;
                            }
                        }
                        else
                        {
                            if (ServerPackets.TryPacket(codes, out InChannel inChannel))
                            {
                                inChannel.Dispatch(packetRead, keyIdentifier, sender, Channel);
                                continue;
                            }
                        }

                        Logging.Alert($"Packet not found: {codes}");
                    }
                    else
                    {
                        Shutdown();
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                    Shutdown();
                }
        }

        private void Shutdown()
        {
            if (!_running)
                return; // pra não haver vários Shutdowns simutâneos enviando o Close mais de uma vez pro player por outros threads, como no Send / Recv, o server enviando Send de um player pra outro e vice versa.
            if (authenticated)
                Logging.User("lost connection.", Channel.Data.Name);
            _running = false;
            Socket.Close();
        }
    }
}