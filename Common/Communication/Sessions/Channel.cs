using Database.Models;

namespace Common.Communication.Sessions
{
    public class Channel
    {
        public Channel(ISession session, UsersModel usersModel)
        {
            Session = session;
            Data = usersModel;
        }

        public ISession Session { get; }

        public UsersModel Data { get; }

        public virtual void Close()
        {
        }
    }
}