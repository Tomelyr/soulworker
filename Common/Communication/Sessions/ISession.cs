using Common.Communication.Messages.Packet;

namespace Common.Communication.Sessions
{
    public interface ISession
    {
        string Address { get; }

        void Send(PacketOut packetOut);

        void Register(Channel channel);
    }
}