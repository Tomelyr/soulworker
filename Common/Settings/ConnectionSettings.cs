namespace Common.Settings
{
    public class ConnectionSettings
    {
        public int ServerId { get; set; }
        public Center Center { get; set; }
        public Center Game { get; set; }
    }

    public class Center
    {
        public string Host { get; set; }
        public int Port { get; set; }
    }

    public class Game
    {
        public string Host { get; set; }
        public int Port { get; set; }
    }
}