using System.IO;
using Newtonsoft.Json;

namespace Common.Settings
{
    public class Settings<T>
    {
        public T Build(string path)
        {
            return JsonConvert.DeserializeObject<T>(File.ReadAllText(path));
        }
    }
}