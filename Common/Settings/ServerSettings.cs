﻿namespace Common.Settings
{
    public class ServerSettings
    {
        public string Host { get; set; }
        public int Port { get; set; }
        public int Backlog { get; set; }
    }
}