﻿using System;
using System.IO;
using System.Threading;
using System.Windows.Forms;
using Common.Communication;
using Common.Server;
using Common.Settings;
using Common.Utilities;
using Database;
using Game.Game;

namespace Game
{
    public static class Emulator
    {
        public static readonly Server Server = new Server();

        public static void Main(string[] args)
        {
            Console.Title = "Soul Worker Game";
            DatabaseFactory.Build(Path.Combine(Application.StartupPath, @"data\database.json"));
            if (DatabaseFactory.Connect())
            {
                Server.Build(new Settings<ServerSettings>().Build(Path.Combine(Application.StartupPath, @"data\network.json")));
                GameData.Build(Path.Combine(Application.StartupPath, @"data\game\data.json"));
                Server.Start();
            }

            while (true) Thread.Sleep(100);
        }

        public static double GetExpToNextLevel(int userLevel)
        {
            var _base = 219;
            var _delta = 2.0;
            return _base * (_delta * (1 + userLevel)) - _base / _delta - 110;
        }

        //2,37


        public static double GetBaseValueByLevel(double _base, double delta, double curve, double userLevel)
        {
            var levelCap = 55;
            var level = userLevel < 1 ? 1 : (userLevel > levelCap ? levelCap : userLevel);
            var x = (level - 1) / (levelCap - 1);
            return _base + Math.Pow(x, curve) * delta;
        }
    }
}