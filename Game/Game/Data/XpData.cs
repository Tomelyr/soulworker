namespace Game.Game.Data
{
    public class XpData
    {
        public int Level { get; set; }

        public int ExpToNextLevel { get; set; }

        public int TotalExp { get; set; }

        public int AcquiredSP { get; set; }

        public int TotalSP { get; set; }
    }
}