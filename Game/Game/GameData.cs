using System.Collections.Generic;
using Common.Log;
using Game.Game.Data;

namespace Game.Game
{
    public static class GameData
    {
        private static Dictionary<int, XpData> XpTable { get; } = new Dictionary<int, XpData>();

        public static void Build(string path)
        {
            Logging.Info("Loading server data...");
            //var deserializeObject = JsonConvert.DeserializeObject<JsonData>(File.ReadAllText(path));
            //foreach (var xpTable in deserializeObject.XpTable)
            //{
            //    XpTable[xpTable.Level] = xpTable.Data;
            //}
        }

        public static bool TryGetXpData(int level, out XpData data)
        {
            return XpTable.TryGetValue(level, out data);
        }

        public static int DamageReduction(int defense, int level)
        {
            return defense / ((defense + 50) * level);
        }
    }
}